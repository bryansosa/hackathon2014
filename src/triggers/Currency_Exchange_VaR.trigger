trigger Currency_Exchange_VaR on Currency_Exchange_VaR__c (before insert) {

    Date Datex;
    Double ConfidenceLevelFactor;
    
    for (Currency_Exchange_VaR__c cev: Trigger.new){
      
        if(cev.Recalculate__c == true){
            
            //--------Performance Calculation-----------
            //
            
            List <Currency_Exchange_Rate__c> cerList1 = [select Date__c, Value__c, Performance__c, Currency_Type__c, n__c
                                                        from Currency_Exchange_Rate__c
                                                        where Date__c <=: cev.Date__c
                                                        and Currency_Type__c =: cev.Currency_To_Evaluate__c
                                                        order by Date__c Desc  limit 950];
            
            Integer i = 0;
            Integer ListSize = cerList1.size();
            
            // numerate
            for(Currency_Exchange_Rate__c c: cerList1){
                if(i < ListSize -1){
                    Integer Nactual = i;
                    Integer Nanterior = Nactual +1;
                    
                    Double ActualCE = c.Value__c;
                    Double AnteriorCE = cerList1[Nanterior].Value__c;
                    
                    c.Performance__c = Math.log(ActualCE / AnteriorCE);
                    i++;
                }
            }
            Update cerList1;
            
            //Prepare Data fo Volatility Calculation ---------------
            //
            
            Datex = cev.Date__c;
            ConfidenceLevelFactor = cev.Factor_of_Confidence_Level__c;
            
            //Clear n
            List <Currency_Exchange_Rate__c> cerList2 = [select Date__c, Value__c, Performance__c, Currency_Type__c, n__c,
                                                         Volatility__c
                                                        from Currency_Exchange_Rate__c
                                                        where Date__c <=: cev.Date__c
                                                        and Currency_Type__c =: cev.Currency_To_Evaluate__c
                                                        order by Date__c Desc  limit 1000];
            
            for(Currency_Exchange_Rate__c c: cerList2){
            	c.n__c = null;
            }
            Update cerList2; 
            
            //Prepare Data
            List <Currency_Exchange_Rate__c> cerList3 = [select Date__c, Value__c, Performance__c, Currency_Type__c, n__c,
                                                         Volatility__c, Calibration_Factor__c
                                                        from Currency_Exchange_Rate__c
                                                        where Date__c <=: cev.Date__c
                                                        and Currency_Type__c =: cev.Currency_To_Evaluate__c
                                                         and Performance__c <> 0
                                                        order by Date__c Desc  limit 615];
            Integer n = 615;
            for(Currency_Exchange_Rate__c c: cerList3){
           		c.n__c = n;
                
                if(Trigger.isInsert){
                    c.Calibration_Factor__c =1;
                }
                
                n--;
                
            }
            Update cerList3;
            
            //Call Volatility
            //
            
            Volatility BatchApex = new Volatility();
            
            BatchApex.query = 'Select Date__c, Value__c, Performance__c, Volatility__c, VaR_of_1_Unit__c, Difference__c, Exchange_Rate_Forecast__c, Currency_Type__c from Currency_Exchange_Rate__c ';
            BatchApex.Datex = cev.Date__c;
            BatchApex.ConfidenceLevelFactor = cev.Factor_of_Confidence_Level__c;
            BatchApex.Horizon = cev.Horizon__c;
            BatchApex.Amount = cev.Amount__c;
            BatchApex.CurrencyToEvaluate = cev.Currency_To_Evaluate__c;
            BatchApex.CalibrationFactor = cev.Multiplication_Factor_of_Back_Test__c;
            BatchApex.VarId = cev.Id;
            
            ID batchprocessid = Database.executeBatch(BatchApex);
            
            cev.Recalculate__c = false;
                
            

            
            
            
            
        }
        
        
      
        
    }
    
    
    
    
    
}