<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Unique_Key</fullName>
        <field>Unique_Key__c</field>
        <formula>text(year( Date__c )) +&quot;-&quot;+ text(month( Date__c )) +&quot;-&quot;+ text(day( Date__c ))+&quot;-&quot;+ text(Currency_Type__c )</formula>
        <name>Unique Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Unique Key</fullName>
        <actions>
            <name>Unique_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
