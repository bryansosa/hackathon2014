global class Volatility implements Database.Batchable<sObject>{
 
    global String query {get;set;}
    global Date Datex {get;set;}
    global Double ConfidenceLevelFactor{get;set;}
    global Double Horizon {get;set;}
    global Double Amount {get;set;}
    global String CurrencyToEvaluate {get;set;}
    global Double CalibrationFactor {get;set;}
    global id VarId {get;set;}
    

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){

       List <Currency_Exchange_Rate__c> CEH1 = new List <Currency_Exchange_Rate__c>();     

       List <Currency_Exchange_Rate__c> CEH2 = [select Date__c, Value__c, Performance__c, Currency_Type__c, n__c,
                                                         Volatility__c, Calibration_Factor__c, Var_of_1_Unit__c
                                                        from Currency_Exchange_Rate__c
                                                        where Date__c <=: Datex
                                                        and Currency_Type__c =: CurrencyToEvaluate
                                                        and Performance__c <> 0
                                                		and n__c <> null
                                                        order by n__c Asc  limit 615];
       
       //Volatility Calculation ---------------------
       double [] PerformanceList = new List <Double>();
       
       for(Currency_Exchange_Rate__c c: CEH2){
           
           if(c.n__c < 365){
               PerformanceList.add(c.Performance__c);
           }

           
           if(c.n__c == 365){
               	 
        //determine the sum of the range of numbers
        Double sum = 0;
        for(Double d : PerformanceList)
        {
            sum += d;
        }

        //determine the mean of the range of numbers
        Double mean = sum / 363;

        //for each number subtract the mean and square the result
        Double squaredDifferencesSum = 0;
        for(Double d : PerformanceList)
        {
            squaredDifferencesSum += Math.pow((d-mean), 2);
        }

        //determine the mean for the squared differences
        Double squaredDifferencesMean = squaredDifferencesSum / 363;

        //determine the standard deviation
        Double standardDeviation = Math.sqrt(squaredDifferencesMean);
               
               c.Volatility__c = standardDeviation;
               c.VaR_of_1_Unit__c = CEH2[integer.valueOf(c.n__c -2)].Value__c * ConfidenceLevelFactor * standardDeviation * Math.sqrt(1);
               c.Exchange_Rate_Forecast__c = CEH2[integer.valueOf(c.n__c -2)].Value__c * double.valueOf(c.Calibration_Factor__c);
               c.Difference_Forecast_vs_Real__c = c.Value__c - c.Exchange_Rate_Forecast__c;
               
               PerformanceList.add(c.Performance__c);
               
               
           }
           
           
           if(c.n__c >= 365){
               
               PerformanceList.remove(0);
                     
               //determine the sum of the range of numbers
        Double sum = 0;
        for(Double d : PerformanceList)
        {
            sum += d;
        }

        //determine the mean of the range of numbers
        Double mean = sum / 363;

        //for each number subtract the mean and square the result
        Double squaredDifferencesSum = 0;
        for(Double d : PerformanceList)
        {
            squaredDifferencesSum += Math.pow((d-mean), 2);
        }

        //determine the mean for the squared differences
        Double squaredDifferencesMean = squaredDifferencesSum / 363;

        //determine the standard deviation
        Double standardDeviation = Math.sqrt(squaredDifferencesMean);
               
               c.Volatility__c = standardDeviation;
               c.VaR_of_1_Unit__c = CEH2[integer.valueOf(c.n__c -2)].Value__c * ConfidenceLevelFactor * standardDeviation * Math.sqrt(1);
               c.Exchange_Rate_Forecast__c = CEH2[integer.valueOf(c.n__c -2)].Value__c * double.valueOf(c.Calibration_Factor__c);
               c.Difference_Forecast_vs_Real__c = c.Value__c - c.Exchange_Rate_Forecast__c;
               
               PerformanceList.add(c.Performance__c);
           }
           
           CEH1.add(c);
           
           
           
       }
        Update CEH1;
       
   }

   global void finish(Database.BatchableContext BC){
       
       System.debug(LoggingLevel.WARN,'Batch Process 1 Finished');
        //Build the system time of now + 20 seconds to schedule the batch apex.
        Datetime sysTime = System.now();
        sysTime = sysTime.addSeconds(20);
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        system.debug(chron_exp);
       
       
        Invoke_Currency_Exchange_VaR BatchApex = new Invoke_Currency_Exchange_VaR();
        //Schedule the next job, and give it the system time so name is unique
        //
          
       Invoke_Currency_Exchange_VaR invoke = new Invoke_Currency_Exchange_VaR();
            
            invoke.query = 'Select Date__c, Value__c, Performance__c, Volatility__c, VaR_of_1_Unit__c, Difference__c, Exchange_Rate_Forecast__c, Currency_Type__c from Currency_Exchange_Rate__c ';
            invoke.Datex = Datex;
            invoke.ConfidenceLevelFactor = ConfidenceLevelFactor;
            invoke.Horizon = Horizon;
            invoke.Amount = Amount;
            invoke.CurrencyToEvaluate = CurrencyToEvaluate;
            invoke.CalibrationFactor = CalibrationFactor;
            invoke.VarId = VarId;
          
       System.schedule('acctBatch2Job' + sysTime.getTime(),chron_exp, invoke);
       
       

   }
}