global class Currency_Exchange_VaR implements Database.Batchable<sObject>{
 
    global String query {get;set;}
    global Date Datex {get;set;}
    global Double ConfidenceLevelFactor{get;set;}
    global Double Horizon {get;set;}
    global Double Amount {get;set;}
    global String CurrencyToEvaluate {get;set;}
    global Double CalibrationFactor {get;set;}
    global id VarId {get;set;}
    

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
       
       List <Currency_Exchange_VaR__c> VarList = new List <Currency_Exchange_VaR__c>();
       
       List <Currency_Exchange_Rate__c> CEList = [select Date__c, Value__c, Performance__c, Currency_Type__c, n__c,
                                                         Volatility__c, Calibration_Factor__c, Var_of_1_Unit__c, Difference_Forecast_vs_Real__c
                                                        from Currency_Exchange_Rate__c
                                                        where Date__c <=: Datex
                                                        and Currency_Type__c =: CurrencyToEvaluate
                                                        and Performance__c <> 0
                                                		and n__c <> null
                                                        order by n__c Asc  limit 615];
       
  

       for(Sobject s : scope)
      {
          Currency_Exchange_VaR__c cev = (Currency_Exchange_VaR__c) s;
          
          IF(
              cev.Date__c == Datex &&
              cev.Horizon__c == Horizon &&
              cev.Factor_of_Confidence_Level__c == ConfidenceLevelFactor &&
              cev.Amount__c == Amount &&
              cev.Currency_To_Evaluate__c == CurrencyToEvaluate          
          ){
              
              if(cev.Ready__c == false){
                  
                  cev.Ready__c = true;
              }
              
              
              //Asign Volatility------------
              //
              
              for( Currency_Exchange_Rate__c cer: CEList){
                  
                  if(cer.Date__c == Datex)
                      cev.Volatility__c = cer.Volatility__c;
                  
              }
              
              //Errors--------
              //
              
              Integer errors= 0;
              for( Currency_Exchange_Rate__c cer: CEList){
                  if(cer.Difference_Forecast_vs_Real__c < 0)
                      errors++;
                  
              }
              
              cev.Errors__c = errors;
              cev.Status_of_the_Model2__c = cev.Status_of_the_Model1__c;
              
              if (errors < 5){
                  cev.Multiplication_Factor_of_Back_Test__c = 1;
              }else {cev.Multiplication_Factor_of_Back_Test__c = null;}
              
  
              
          }
          
          VarList.add(cev);
          
      }      
       
       
   }

   global void finish(Database.BatchableContext BC){
       

       
       

   }
}