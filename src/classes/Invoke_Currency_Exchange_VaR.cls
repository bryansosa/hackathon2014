global class Invoke_Currency_Exchange_VaR  implements Schedulable{   
        
    global String query {get;set;}
    global Date Datex {get;set;}
    global Double ConfidenceLevelFactor{get;set;}
    global Double Horizon {get;set;}
    global Double Amount {get;set;}
    global String CurrencyToEvaluate {get;set;}
    global Double CalibrationFactor {get;set;}
    global id VarId {get;set;}
    
    
    
    
    global void execute(SchedulableContext ctx){
           Currency_Exchange_VaR var = new Currency_Exchange_VaR();
            
            var.query = 'Select Status_of_the_Model1__c, Status_of_the_Model2__c, Errors__c, Volatility__c, Multiplication_Factor_of_Back_Test__c, Date__c, Horizon__c, Factor_of_Confidence_Level__c, Confidence_Level__c, Amount__c, Ready__c, Currency_To_Evaluate__c from Currency_Exchange_VaR__c';
            var.Datex = Datex;
            var.ConfidenceLevelFactor = ConfidenceLevelFactor;
            var.Horizon = Horizon;
            var.Amount = Amount;
            var.CurrencyToEvaluate = CurrencyToEvaluate;
            var.CalibrationFactor = CalibrationFactor;
            var.VarId = VarId;
        
        ID batchprocessid = Database.executeBatch(var);
   }   
}